#include <stdio.h>
#include <stdlib.h>

int main() {
	char *name = getenv("NAME");
	if (name == NULL) {
		fprintf(stderr, "error: set NAME environmental variable.\n");
		return 1;
	}
	printf("Hello, %s!\n", name);
}
